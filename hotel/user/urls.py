from django.conf.urls import url,include
from user import views

urlpatterns = [
   url(r'^login',views.login,name='user_login'),
   url(r'^logout',views.logout,name='user_logout'),
   url(r"^register",views.register,name='user_register' ),
   url(r'^reserve',views.reserve,name='user_reserve'),



]
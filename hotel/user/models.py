from django.db import models
from django.contrib.auth import get_user_model
from django.utils.timezone import now
# Create your models here.、
User = get_user_model()
class users(models.Model):
    '''
    用户订餐
    '''
    user = models.ForeignKey(User, verbose_name='用户', on_delete=None, null=True)
    Date = models.DateTimeField(default=now, verbose_name='订餐日期')
    Dinner = models.CharField(choices=(('Dinner','晚餐'),('Breakfast','早餐'),('Lunch','午餐')),verbose_name='订餐时间',max_length=50,default='Dinner')
    Guests = models.IntegerField(default=1,verbose_name='订餐人数',choices=((1,1),(2,2),(3,3),('更多','更多')))
    Suggestions = models.TextField(max_length=2000, default='Message',verbose_name='其他')

    def __str__(self):
        return "{}-{}-{}:{}:{}:{}".format(self.Date.year,self.Date.month,self.Date.day,self.Date.hour,self.Date.minute,self.Date.second)
    class Meta:
        verbose_name = '订餐用户'
        verbose_name_plural = verbose_name
from django import forms
from django.contrib.auth import get_user_model

from user.models import users


User = get_user_model()



class UserForms(forms.ModelForm):
    '''
    表单类，和数据库没有关系
    '''
    #required= True表示不能为空

    # =================第一种方法====================
    # username = forms.CharField(max_length=20,min_length=2,required=True)
    # password = forms.CharField(max_length=20,min_length=8,required=True)

    #==================第二种方法==================
    username = forms.CharField(max_length=20)

    class Meta:
        model = User
        fields = ("password",'email')
        #额外的数据
        widgets = {
            'password': forms.PasswordInput()  #隐藏密码
        }





class UserRegForm(forms.ModelForm):
    """
    表单类,注册
    """
    class Meta:
        model = User
        fields = ("username","password","email")

        widgets = {
            'password': forms.PasswordInput()
        }



class Reserve(forms.ModelForm):
    '''
    订餐显示表单
    '''
    class Meta:
        model = users
        fields = ('Date','Dinner','Guests','Suggestions')

class Reservevlaue(forms.ModelForm):
    '''
    订餐验证表单
    '''
    class Meta:
        model = users
        fields = ('user','Date','Dinner','Guests','Suggestions')
from django.shortcuts import render

# Create your views here.
from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate,login as user_login,logout as user_logout,get_user_model
from django.contrib.auth import get_user_model
from user.froms import UserForms,UserRegForm,Reservevlaue #调用表单类
# Create your views here.


from django.contrib import messages

def login(request):
    '''
    用户登录界面
    :param request:
    :return:

    '''

    #==========第一种方法==============
    # username = request.POST['username']
    # if (not username) or (username == None) :
    #     return HttpResponse('用户不能为空')
    #
    # userpassword = request.POST['password']
    # if (not userpassword) or (userpassword == None) :
    #     return HttpResponse('密码不能为空')
    #
    # date = UserForms(request.POST) #验证表单，生成一个类
    # if not date.is_valid():        #验证数据格式
    #     return HttpResponse('数据不合法')
    #
    # res = date.cleaned_data   #验证合法数据
    #
    # user = authenticate(request,username =request.POST['username'],password = request.POST['password'])
    # # # login(user,request)


    # if user is None:  # 验证密码是否在数据库中
    #     return HttpResponse('没注册')

    # user_login(request,user)  #获取用户，生成sessionid加密放入django_lession库
    # return HttpResponse('OK')
    #===================第二种方法===========================================

    url_path = request.META['HTTP_REFERER']
    date = UserForms(request.POST)  #验证表单，生成一个类
    if not date.is_valid():         #验证数据格式

        messages.error(request, "数据格式有误")
        return redirect(url_path)

    res = date.cleaned_data         #验证合法数据
    user = authenticate(**res)
    if user is None:                #验证密码是否在数据库中

        messages.error(request, "用户不存在或密码不准确")
        return redirect(url_path)

    user_login(request,user)        #获取用户，生成sessionid加密放入django_lession库

    # url_path = request.META['HTTP_REFERER'] #获取浏览器请求网址
    # print(url_path)
    return redirect(url_path) #重定向网页


def logout(request):
    '''
    退出登录
    :param request:
    :return:
    '''
    user_logout(request)
    url_path = request.META['HTTP_REFERER']  # 获取浏览器请求网址

    return redirect(url_path)  # 重定向网页





User = get_user_model()

def register(request):
    """
    注册 接口
    :param request:
    :return:
    """
    url_path = request.META['HTTP_REFERER'] #获取浏览器请求网址
    data = UserRegForm(request.POST)
    if not data.is_valid():
        messages.error(request, "数据有误或用户已存在")
        return redirect(url_path)

    res = data.cleaned_data
    user = User.objects.create_user(**res)
    user_login(request, user)
    messages.success(request, "注册成功")
    return redirect('order_index')


def reserve(request):
    """
    验证表单
    :param request:
    :return:
    """
    url_source = request.META['HTTP_REFERER']
    comment_data = dict()
    date = request.POST.get('Date')
    dinner = request.POST.get('Dinner')
    guests = request.POST.get('Guests')
    suggestions = request.POST.get('Suggestions')

    comment_data['user'] = request.user.id
    comment_data['Date'] = date
    comment_data['Dinner'] = dinner
    comment_data['Guests'] = guests
    comment_data['Suggestions'] = suggestions

    data = Reservevlaue(comment_data)
    if not data.is_valid():
        messages.error(request, "输入有误")
        return redirect(url_source)
    data.save()
    messages.success(request,'预约成功')
    return redirect(url_source)

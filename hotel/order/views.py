
from user.froms import UserForms,UserRegForm,Reserve
from django.shortcuts import render,get_object_or_404
from order.models import single_list
# Create your views here.
def index(request):
    '''
    首页
    :param request:
    :return:
    '''
    userforms = UserForms()
    userregform = UserRegForm()
    users_order = Reserve()
    single = single_list.objects.order_by('-id')

    return render(request,'index.html',{'userform':userforms,'reg':userregform,'use_order':users_order,'single':single})


def single(request,path):
    '''
    详细页面
    :param request:
    :return:
    '''
    userforms = UserForms()
    userregform = UserRegForm()
    users_order = Reserve()

    id = get_object_or_404(single_list.objects.filter(id=path))


    return render(request,'single.html',{'userform':userforms,'reg':userregform,'use_order':users_order,'id':id})




def about(request):
    '''
    关于
    :param request:
    :return:
    '''
    userforms = UserForms()
    userregform = UserRegForm()
    users_order = Reserve()

    return render(request, 'about.html', {'userform': userforms, 'reg': userregform, 'use_order':users_order})

def blog(request):
    '''

    :param request:
    :return:
    '''
    userforms = UserForms()
    userregform = UserRegForm()
    users_order = Reserve()

    return render(request, 'blog.html', {'userform': userforms, 'reg': userregform, 'use_order':users_order})

def codes(request):
    '''

    :param request:
    :return:
    '''
    userforms = UserForms()
    userregform = UserRegForm()
    users_order = Reserve()

    return render(request, 'codes.html', {'userform': userforms, 'reg': userregform, 'use_order':users_order})


def contact(request):
    '''

    :param request:
    :return:
    '''
    userforms = UserForms()
    userregform = UserRegForm()
    users_order = Reserve()

    return render(request, 'contact.html', {'userform': userforms, 'reg': userregform, 'use_order':users_order})


def chuce(request):
    '''
    注册
    :param request:
    :return:
    '''
    userforms = UserForms()
    userregform = UserRegForm()

    return render(request, 'ZHUCE.html', {'userform': userforms, 'reg': userregform})

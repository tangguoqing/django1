from django.db import models
from uuid import uuid4

def upload_image(instance,filename):
    '''
    对上传图片地址处理
    :param instance:
    :param filename:
    :return:
    '''


    image_end = filename.split('.')  #将图片以.分割
    res_filename = uuid4().hex   #生成16位唯一值
    return "{}.{}".format(res_filename,image_end[-1])




class single_list(models.Model):
    '''


    '''
    name = models.CharField(max_length=50,unique=True)
    text = models.CharField(max_length=250)

    image = models.ImageField(upload_to=upload_image,blank= True)
    time = models.DateField(auto_now=True)
    def __str__(self):
        return self.name
from django.conf.urls import url
from order import views


urlpatterns = [
    url(r'^index/$',views.index,name='order_index'),
    url(r'single/(?P<path>.*)/$',views.single,name='single'),
    url(r'single/$',views.single,name='single'),
    url(r'^chuce/$',views.chuce,name='order_chuce'),
    url(r'^about/$',views.about,name= 'about'),
    url(r'^blog/$',views.blog,name= 'blog'),
    url(r'^codes/$',views.codes,name= 'codes'),
    url(r'^contact/$',views.contact,name= 'contact')
    ]
